# gnome-keys-reminder

This is a python script that can be useful if you have many custom keybindings
in GNOME, MATE or Cinnamon desktop environment. It shows you a window with
enumerated keybindings.

## Dependencies

This script needs for working python3, python3-gi and dconf-cli packages (how
it named in Debian and derivatives).

## How to use it

Clone this repo in some directory, e. g. downloads:

`$ git clone https://gitlab.com/bergentroll/gnome-keys-reminder`

go to the new directory:

`$ cd gnome-keys-reminder`

and do:

`$ make install`

Last command will install script to your ~/.local dir. Do not run
`make install` from superuser!

To remove this script in downloaded folder do:

`$ make uninstall`

That's all, folks!
