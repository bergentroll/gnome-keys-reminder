#!/usr/bin/env python3

# Script shows you custom keybindings in GNOME, MATE or Cinnamon
# Copyright (C) 2017 Anton Karmanov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class keybindings:

    keybindingsList = str()

    def getKeybindings(self):

        desktop = os.getenv("XDG_CURRENT_DESKTOP")
        if desktop == "MATE":
            gconf_path = "/org/mate/desktop/keybindings/"
        elif desktop == "X-Cinnamon":
            gconf_path = (
                "/org/cinnamon/desktop/keybindings/custom-keybindings/")
        elif desktop in ("GNOME", "GNOME-Classic:GNOME"):
            gconf_path = (
                "/org/gnome/settings-daemon/plugins/media-keys/" +
                "custom-keybindings/")
        else:
            self.keybindingsList = (
                "Sorry, it seems not compatible environment.")
            return()

        # Get list of entries in dconf.
        binds_list = os.popen(
            "dconf list " + gconf_path
        ).read().splitlines()
        output_list = list()

        # Get names and values of bindings.
        max_len = 0
        for bind in binds_list:
            name = os.popen(
                "dconf read " + gconf_path + bind + "name"
            ).read()[1:-2]
            bind = os.popen(
                "dconf read " + gconf_path + bind + "binding"
            ).read()[1:-2]
            name_len = len(name)
            if name_len > max_len:
                max_len = name_len
            output_list.append((name, bind, name_len))

        # Make output literal.
        output = str()
        for item in output_list:
            extra_spaces = max_len - item[2]
            output += item[0] + "_" * extra_spaces + " — " + item[1] + "\n"
        self.keybindingsList = output[0:-1]


class Handler:

    def __init__(self):
        self.window_is_hidden = False
        keybindings.getKeybindings()
        label1 = builder.get_object("label1")
        label1.set_text(keybindings.keybindingsList)

    def onQuit(self, *args):
        Gtk.main_quit()


builder = Gtk.Builder()
try:
    builder.add_from_file("gnome-keys-reminder.glade")
except:
    builder.add_from_file(
        os.popen("xdg-user-dir").read()[0:-1] +
        "/.local/share/gnome-keys-reminder/gnome-keys-reminder.glade")

keybindings = keybindings()

builder.connect_signals(Handler())

window = builder.get_object('root')

window.show_all()

Gtk.main()
