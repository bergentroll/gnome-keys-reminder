#!/usr/bin/make -f

SCRIPT = gnome-keys-reminder
install:
	mkdir -p ~/.local/bin
	mkdir -p ~/.local/share/$(SCRIPT)
	cp $(SCRIPT).py ~/.local/bin/
	cp $(SCRIPT).glade ~/.local/share/$(SCRIPT)/
uninstall:
	rm ~/.local/bin/$(SCRIPT).py
	rm -rf ~/.local/share/$(SCRIPT)
